import org.junit.jupiter.api.Test;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Testcontainers
public class MixedLifecycleIT {

    @SuppressWarnings("rawtypes")
    @Container
    private PostgreSQLContainer postgresqlContainer = new PostgreSQLContainer("postgres:latest")
            .withDatabaseName("foo")
            .withUsername("foo")
            .withPassword("secret");

    @Test
    public void test() {
        assertTrue(postgresqlContainer.isRunning());
    }
}